-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main
  ( main
  )
where

import qualified Database.SQLite.Simple        as S
import           Options.Applicative

import           Heracles.Database
import           Heracles.Types.DbRow


data Opts = Opts
  { dbFile          :: FilePath
  , resetProcessing :: Bool
  } deriving Show

optionsParser :: Parser Opts
optionsParser =
  Opts
    <$> strOption (long "db" <> help "The name of the SQLite database file to track results")
    <*> switch (short 'p' <> long "processing" <> help "Also reset rows that are processing")

main :: IO ()
main = do
  let
    oa = info
      (optionsParser <**> helper)
      (  fullDesc
      <> progDesc "e.g. heracles-wipe-failures --db=asvo.sqlite -p"
      <> header
           "heracles-wipe-failures - Reset the SQLite database rows that have failed (and optionally those that are labelled 'processing')."
      )
    oaPrefs = prefs (showHelpOnEmpty <> disambiguate)
  Opts {..} <- customExecParser oaPrefs oa

  rows      <- S.withConnection dbFile getAllRows

  let cond DbRow { _status } =
        _status == Failed || _status == DownloadError || (resetProcessing && _status == Processing)
  let filtered = _obsid <$> filter cond rows
  S.withConnection dbFile $ resetObsids filtered
