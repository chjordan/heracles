-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main
  )
where

import           Control.Monad
import qualified Data.Text.Lazy.IO             as TLIO
import qualified Database.SQLite.Simple        as S
import           Options.Applicative
import           Rainbow
import qualified System.Directory              as D

import           Heracles.Database
import           Heracles.Internal

data Args = Args
  { oDbFile        :: FilePath
  , oDataDirectory :: FilePath
  , oDryRun        :: Bool
  , oVerbose       :: Bool
  , oDebug         :: Bool
  } deriving Show

optionsParser :: Parser Args
optionsParser =
  Args
    <$> strOption
          (  long "db"
          <> help "The name of the SQLite database file to track results"
          <> value "/data/asvo.sqlite"
          <> showDefault
          )
    <*> strOption
          (  short 'd'
          <> long "data-directory"
          <> help "The working directory"
          <> value "/data"
          <> showDefault
          )
    <*> switch (short 'n' <> long "dry-run" <> help "Display any updates, but do not commit them")
    <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
    <*> switch (long "debug" <> help "Display debug messages")

main :: IO ()
main = do
  let
    oa = info
      (optionsParser <**> helper)
      (  fullDesc
      <> progDesc "e.g. heracles-update-database --db=/data/asvo.sqlite --data-directory=/data"
      <> header
           "heracles-update-database - Update a SQLite database managed by heracles against a specified data directory. This is useful when wanting to sync the database against a data directory which has manually copied completed jobs."
      )
    oaPrefs = prefs disambiguate
  opts          <- customExecParser oaPrefs oa

  dbFile        <- D.makeAbsolute $ oDbFile opts
  dataDirectory <- D.makeAbsolute $ oDataDirectory opts

  -- General sanity checks.
  D.doesDirectoryExist dataDirectory >>= flip
    unless
    (errorWithoutStackTrace $ "Specified data directory (" <> dataDirectory <> ") does not exist!")
  D.setCurrentDirectory dataDirectory

  -- Always verbose when debugging.
  let verbose = oDebug opts || oVerbose opts

  terminalCapabilities <- byteStringMakerFromEnvironment

  -- Initialise/create the SQLite database, and read in its contents.
  databaseContents     <- S.withConnection dbFile $ \conn -> do
    createDatabase conn
    databaseToHashMap conn

  -- Check the directories available in the `dataDirectory`; do their statuses
  -- reflect those in the database? Update if necessary.
  updates <- updateDatabaseAgainstDataDirectory databaseContents dataDirectory
  if null updates
    then putStrLn "No updates found!"
    else do
      if oDryRun opts
        then putStrLn "DRY RUN: No changes performed."
        else S.withConnection dbFile
          $ \conn -> S.withTransaction conn $ forM_ updates $ insertOrUpdateDBRow conn

      printMessageNoEnv
        terminalCapabilities
        [ chunk (tlshow $ length updates) & fore yellow
        , chunk " obsids updated from directories found in the data directory.\n"
        ]

      when (verbose || oDryRun opts) $ mapM_ TLIO.putStrLn $ tlshow <$> updates
