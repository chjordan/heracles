-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# OPTIONS_GHC -Wno-partial-fields #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main
  ( main
  )
where

import           Control.Concurrent.Async
import           Control.Concurrent.MVar
import           Control.Concurrent.STM
import           Control.Exception.Safe                   ( tryIO )
import           Control.Monad
import           Control.Monad.Reader                     ( runReaderT )
import           Data.ByteString.Char8                    ( ByteString )
import qualified Data.ByteString.Char8         as B8
import qualified Data.Map.Lazy                 as M
import           Data.Maybe                               ( fromJust
                                                          , fromMaybe
                                                          )
import qualified Data.Text.Lazy                as TL
import           Data.Version                             ( showVersion )
import qualified Database.SQLite.Simple        as S
import           Network.HTTP.Client                      ( CookieJar )
import           Network.HostName                         ( getHostName )
import qualified Network.Simple.TCP            as NS
import           Options.Applicative
import qualified Options.Applicative.Help
import           Rainbow
import qualified System.Directory              as D
import           System.Environment                       ( setEnv )
import           System.Exit                              ( exitSuccess )

import           GiantSquid.ASVO                          ( connectToAsvo )
import           Heracles
import           Heracles.Database
import           Heracles.Internal
import           Heracles.Types.Client                    ( ClientEnv(..) )
import           Heracles.Types.Server                    ( ServerEnv(..) )
import qualified Paths_giant_squid             as GS
import           Paths_heracles                           ( version )

data Args = ClientArgs
  { serverIP      :: String
  , serverPortArg :: Int
  , dataDirPath   :: FilePath
  , checkScript   :: Maybe FilePath
  , runScript     :: FilePath
  , cleanupScript :: FilePath
  , timeLimitMins :: Int
  , verboseArg    :: Bool
  , debugArg      :: Bool
  , printVersion  :: Bool
  }
  | ServerArgs
  { mServerIP     :: Maybe String
  , serverPortArg :: Int
  , dataDirPath   :: FilePath
  , dbFilePath    :: FilePath
  , bufferSize    :: Int
  , verboseArg    :: Bool
  , debugArg      :: Bool
  }
  deriving Show

optionsParser :: Parser Args
optionsParser =
  (   ClientArgs
    <$> strOption
          (  long "ip"
          <> help
               "The IP address of the machine running the heracles server, e.g. \"192.168.0.10\", \"localhost\"."
          )
    <*> option
          auto
          (  long "port"
          <> help "The port that the heracles server is listening on."
          <> value 6200
          <> showDefault
          )
    <*> strOption
          (  short 'd'
          <> long "data-directory"
          <> help
               "The working directory. Make sure this directory has enough space to hold raw MWA data."
          )
    <*> optional
          (strOption
            (long "check" <> help
              "The path to the check script, which checks that software dependencies are working."
            )
          )
    <*> strOption
          (short 's' <> long "script" <> help "The path to the script to be run on MWA data.")
    <*> strOption
          (  long "cleanup"
          <> help
               "The path to the cleanup script used after the run script. It should delete any files that you don't want to keep."
          )
    <*> option
          auto
          (  short 't'
          <> long "time-limit"
          <> help "Kill processing jobs that run longer than this [minutes]"
          <> value 180
          <> showDefault
          )
    <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
    <*> switch (long "debug" <> help "Display debug messages")
    <*> switch (short 'V' <> long "version" <> help "Display the software versions used")
    )
    <|> hsubparser
          (command
            "server"
            (info
              (   ServerArgs
              <$> optional
                    (strOption (long "ip" <> help "The IP address to listen on. Optional."))
              <*> option
                    auto
                    (long "port" <> help "The port to listen on" <> value 6200 <> showDefault)
              <*> strOption
                    (short 'd' <> long "data-directory" <> help
                      "The working directory. Also where obsid .txt files are consumed."
                    )
              <*> strOption
                    (long "db" <> help "The name of the SQLite database file to track results.")
              <*> option
                    auto
                    (  short 'b'
                    <> long "buffer-size"
                    <> help "The number of observations to be buffered by the ASVO."
                    <> value 30
                    <> showDefault
                    )
              <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
              <*> switch (long "debug" <> help "Display debug messages")
              )
              (  headerDoc
                  (  Just
                  $  Options.Applicative.Help.bold "heracles server"
                  <> ": Orchestrate job farming with MWA data"
                  )
              <> progDesc
                   "e.g. heracles server -d ~/WORKING_DIR/MWA/ASVO_CALIBRATE --db asvo.db"
              )
            )
          )

data Common = Common
  { dataDirectory         :: String
  , cookies               :: Maybe CookieJar
  , mTerminalCapabilities :: Maybe (Chunk TL.Text -> [ByteString] -> [ByteString])
  }

-- | Do some things that both the client and server would do, and return things
-- that they both want.
runCommon :: Bool -> Bool -> Common -> IO Common
runCommon beVerbose debugging Common {..} = do
  let verbose = beVerbose || debugging

  -- Ensure that the data directory exists, and set it as the PWD.
  absDataDirectory <- D.makeAbsolute dataDirectory
  D.doesDirectoryExist absDataDirectory >>= flip
    unless
    (  errorWithoutStackTrace
    $  "Specified data directory ("
    <> absDataDirectory
    <> ") does not exist!"
    )
  D.setCurrentDirectory absDataDirectory

  -- Get the terminal capabilities to display colours, if possible.
  terminalCapabilities <- byteStringMakerFromEnvironment

  -- Connect to the ASVO and keep the login information.
  when verbose $ putStr "Connecting to ASVO... "
  cookieJar <- connectToAsvo
  when verbose $ putStrLn "done."

  return $ Common { dataDirectory         = absDataDirectory
                  , cookies               = Just cookieJar
                  , mTerminalCapabilities = Just terminalCapabilities
                  }

linkedAsync :: IO a -> IO ()
linkedAsync = link <=< async

main :: IO ()
main = do
  let
    oa = info
      (optionsParser <**> helper)
      (  fullDesc
      <> progDesc
           "e.g. heracles --db=/data/asvo.sqlite --data-directory=/data --time-limit=120 --buffer-size=30"
      <> header
           "heracles - Automatic download-process-delete handler for MWA data. Consumes .txt files containing obsids in the specified data directory. Press the Enter key to exit gracefully."
      )
    oaPrefs = prefs (showHelpOnEmpty <> disambiguate)

  args <- customExecParser oaPrefs oa

  case args of
    ClientArgs {..} -> do
      when printVersion $ do
        putStrLn $ unlines
          [ "heracles version: " <> showVersion version
          , "Source: https://gitlab.com/chjordan/heracles"
          , "giant-squid version: " <> showVersion GS.version
          , "Source: https://gitlab.com/chjordan/giant-squid"
          ]
        exitSuccess

      -- Always verbose when debugging.
      let verbose    = verboseArg || debugArg
          debug      = debugArg
          serverPort = show serverPortArg

      -- Sanity checks.
      -- Validate all of the scripts.
      -- Check script is optional.
      checkScriptPath <- case checkScript of
        Just s -> do
          s' <- D.makeAbsolute s
          D.doesFileExist s' >>= flip
            unless
            (errorWithoutStackTrace $ "Specified check script (" <> s' <> ") does not exist!")
          return $ Just s'
        Nothing -> return Nothing

      -- Run script is mandatory.
      runScriptPath <- do
        s <- D.makeAbsolute runScript
        D.doesFileExist s >>= flip
          unless
          (errorWithoutStackTrace $ "Specified run script (" <> s <> ") does not exist!")
        return s

      -- Cleanup script is mandatory.
      cleanupScriptPath <- do
        s <- D.makeAbsolute cleanupScript
        D.doesFileExist s >>= flip
          unless
          (errorWithoutStackTrace $ "Specified cleanup script (" <> s <> ") does not exist!")
        return s

      -- Check that we can obtain the current machine's hostname.
      hn <- getHostName
      when debug $ putStrLn $ "Machine hostname: " <> hn

      -- If we've been supplied with a "check" script, then run it.
      _ <- case checkScriptPath of
        Nothing -> return ()
        Just c  -> checkProgramsRun [c] >>= \p ->
          unless (null p)
            $  errorWithoutStackTrace
            $  "The check script did not run correctly:\n\n"
            <> unlines ((\(program, stderr) -> program <> "\n\n" <> stderr) <$> p)

      -- Attempt to connect to the server. Let this line fail if we can't
      -- connect, because the server details could be legitimately wrong.
      sock <- tryIO (NS.connectSock serverIP serverPort) >>= \case
        Left  _         -> errorWithoutStackTrace "Could not connect to server. Is it running?"
        Right (sock, _) -> return sock

      correct <- serverRespondsCorrectly sock
      unless correct $ errorWithoutStackTrace
        "Server did not respond correctly. Is there a networking problem?"
      when verbose $ putStrLn "Connection to server established."
      when debug $ putStrLn "Obtaining MWA_ASVO_API_KEY from server..."
      NS.send sock "MWA ASVO key?"
      mwaAsvoKey <- NS.recv sock 4096 >>= \case
        Nothing -> error "Server failed to send MWA_ASVO_API_KEY!"
        Just k  -> return k
      setEnv "MWA_ASVO_API_KEY" $ B8.unpack mwaAsvoKey

      -- Send the server our heracles version; if it does not match, we need to
      -- exit.
      when debug $ putStr "Checking server's version of heracles... "
      NS.send sock "heracles version?"
      serverHeraclesVersion <- NS.recv sock 4096 >>= \case
        Just v -> return v
        Nothing ->
          error
            "Server failed to send its heracles version! Check that the server's heracles is up to date."
      when debug $ B8.putStrLn serverHeraclesVersion
      when (B8.unpack serverHeraclesVersion /= showVersion version)
        $ error "Server's version of heracles does not match this version of heracles!"

      NS.closeSock sock

      Common {..} <- runCommon verbose debug
        $ Common { dataDirectory = dataDirPath, cookies = Nothing, mTerminalCapabilities = Nothing }
      exitNowTVar <- newTVarIO False
      let cookieJar            = fromJust cookies
          terminalCapabilities = fromJust mTerminalCapabilities
          runEnvironment       = ClientEnv { .. }

      printMessageNoEnv
        terminalCapabilities
        [ chunk "heracles" & bold
        , chunk ": "
        , chunk "Client mode running.\n" & fore green
        , chunk "Press the Enter key to exit gracefully, or kill with Ctrl-C at any time.\n"
        ]

      -- Start a thread which listens for the Enter key. This controls cleanly
      -- exiting heracles.
      _ <- linkedAsync $ runReaderT exitOnEnter runEnvironment

      -- Loop until we get the exit message.
      runReaderT clientLoop runEnvironment

    ServerArgs {..} -> do
      -- Always verbose when debugging.
      let verbose    = verboseArg || debugArg
          debug      = debugArg
          serverPort = show serverPortArg
          serverIP   = fromMaybe "" mServerIP

      Common {..} <- runCommon verbose debug
        $ Common { dataDirectory = dataDirPath, cookies = Nothing, mTerminalCapabilities = Nothing }
      let cookieJar            = fromJust cookies
          terminalCapabilities = fromJust mTerminalCapabilities

      dbFile          <- D.makeAbsolute dbFilePath

      -- A HashMap of the ASVO queue.
      asvoQueueTVar   <- newTVarIO M.empty
      -- Utilise an MVar to read/write to the SQLite database, to avoid race conditions.
      databaseMVar    <- newEmptyMVar
      -- A in-memory copy of the SQLite database to be passed around for reading/writing.
      jobStatusesTVar <- newTVarIO M.empty
      -- Indicate whether we should exit or not (currently unused by the
      -- server).
      exitNowTVar     <- newTVarIO False

      let runEnvironment = ServerEnv { .. }

      -- Initialise/create the SQLite database, and populate the job status TVar.
      databaseContents <- S.withConnection dbFile $ \conn -> do
        createDatabase conn
        databaseToHashMap conn
      atomically $ writeTVar jobStatusesTVar databaseContents

      -- Launch the thread which updates the database. This is the only place that
      -- the database is updated, so there's no chance of race conditions!
      _ <- linkedAsync $ runReaderT (updateDatabaseThread dbFile) runEnvironment

      -- Launch the thread which periodically scans for text files with obsids.
      _ <- linkedAsync $ runReaderT readObsidFiles runEnvironment

      -- Launch the thread which periodically updates the ASVO queue.
      -- Sleep for a few seconds to let `readObsidFiles` finish.
      _ <- sleep 3
      _ <- linkedAsync $ runReaderT updateAsvoStatus runEnvironment

      -- Finally, listen for incoming socket connections.
      when debug $ putStrLn $ "Listening on address " <> serverIP <> ", port " <> serverPort <> "."

      printMessageNoEnv
        terminalCapabilities
        [ chunk "heracles" & bold
        , chunk ": "
        , chunk "Server mode running.\n" & fore green
        , chunk "Kill with Ctrl-C at any time.\n"
        ]
      -- TODO: Use the specified IP, not just anything.
      -- NS.serve (NS.Host serverIP) serverPort
      NS.serve NS.HostAny serverPort $ \(connectionSocket, remoteAddr) -> do
        when verbose $ putStrLn $ concat
          [show remoteAddr, " has connected on socket ", show connectionSocket, "."]
        runReaderT (serverLoop connectionSocket) runEnvironment
