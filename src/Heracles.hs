-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Heracles
  ( printMessage
  , printLogMessage
  , printLogMessage'
  , exitOnEnter
  , readObsidFiles
  , updateAsvoStatus
  , extractJob
  , resetJobToDownloading
  , serverLoop
  , clientLoop
  , updateDatabaseThread
  )
where

import           Conduit                                  ( MonadUnliftIO
                                                          , PrimMonad
                                                          )
import           Control.Concurrent.MVar
import           Control.Monad.Reader                     ( MonadIO
                                                          , MonadReader
                                                          , ask
                                                          , liftIO
                                                          )
import qualified Data.ByteString.Char8         as B8
import qualified Data.HashMap.Lazy             as HM
import           Data.List                                ( isSuffixOf )
import qualified Data.Map.Lazy                 as M
import           Data.Serialize                           ( decode
                                                          , encode
                                                          )
import qualified Data.Set                      as S
import qualified Data.Text.IO                  as TIO
import qualified Data.Text.Lazy                as TL
import           Data.Time.Clock                          ( UTCTime
                                                          , diffUTCTime
                                                          , getCurrentTime
                                                          )
import           Data.Time.Clock.Serialize                ( )
import qualified Data.Vector                   as V
import           Data.Version                             ( showVersion )
import           Database.SQLite.Simple
import           Network.HTTP.Client               hiding ( withConnection )
import           Network.HostName                         ( getHostName )
import qualified Network.Simple.TCP            as NS
import           Rainbow
import           Safe                                     ( headMay )
import qualified System.Directory              as D
import           System.Environment                       ( getEnv )
import           System.Exit
import qualified System.FilePath.Glob          as G
import           System.FilePath.Posix                    ( (</>) )
import           System.IO.Error                          ( isEOFError )
import qualified System.Process                as P

import           GiantSquid

import           Heracles.Database
import           Heracles.Imports
import           Heracles.Internal
import           Heracles.Types
import qualified Heracles.Types                as HT
import           Heracles.Types.Client
import           Heracles.Types.DbRow
import qualified Heracles.Types.DbRow          as DbRow
import           Heracles.Types.Reader
import           Heracles.Types.Server
import           Paths_heracles                           ( version )


printJobResult :: (HeraclesReader a, MonadReader a m, MonadIO m) => Bool -> DbRow -> m ()
printJobResult endWithNewLine DbRow {..} =
  let (obsidColour, reason') = case _status of
        DbRow.Succeeded -> (green, "")
        _               -> (red, " (" <> _message <> ")")
      reason = reason' `TL.append` if endWithNewLine then "\n" else ""
  in  printLogMessage
        [ chunk (tlshow _obsid) & fore obsidColour
        , chunk " "
        , chunk $ tlshow _status
        , chunk " processing"
        , chunk reason
        ]

readObsidFiles :: (MonadReader ServerEnv m, MonadIO m) => m ()
readObsidFiles = do
  ServerEnv {..} <- ask

  -- Add and delete any .txt files containing *only* obsids in the data directory.
  textFiles      <- filter (".txt" `isSuffixOf`) <$> liftIO (D.listDirectory dataDirectory)
  when debug $ liftIO . putStrLn $ "In readObsidFiles: textFiles: " <> show textFiles
  unless (null textFiles) $ do
    (textFilesWithObsids, newObsids) <-
      fmap (fmap (S.fromList . concat) . unzip . catMaybes) <$> forM textFiles $ \f -> do
        when debug
          $   liftIO
          .   putStrLn
          $   "In readObsidFiles: Attempting to convert "
          <>  dataDirectory
          </> f
          <>  " to obsids..."
        maybeObsids <- fmap sequence <$> fileToObsids $ dataDirectory </> f
        case maybeObsids of
          Nothing -> return Nothing
          Just os -> return $ Just (f, os)

    when debug $ liftIO . putStrLn $ "In readObsidFiles: newObsids: " <> show newObsids
    -- Don't continue if there are no new observations.
    unless (S.null newObsids) $ do
      -- Check if any obsids in `newObsids` are already in the database.
      -- If so, just reset these rows, and only insert the others.
      (common, uncommon, updatedDbRows) <- updateDatabaseFromFileContents jobStatusesTVar newObsids

      -- Update the SQLite database.
      liftIO $ putMVar databaseMVar updatedDbRows

      -- Delete the files that had obsids.
      mapM_ (liftIO . D.removeFile) $ (</>) dataDirectory <$> textFilesWithObsids
      printMessage
        [chunk "Added new obsids from files: ", chunk $ TL.unwords $ tlshow <$> textFilesWithObsids]
      unless (S.null uncommon) $ printMessage
        [chunk "Added ", chunk (tlshow $ S.size uncommon) & fore blue, chunk " obsids."]
      unless (S.null common) $ printMessage
        [chunk "Reset ", chunk (tlshow $ S.size common) & fore yellow, chunk " obsids."]

  sleep 60
  readObsidFiles

-- | Update the 'TVar' containing the ASVO jobs status, and queue up more jobs
-- if necessary.
updateAsvoStatus :: (MonadReader ServerEnv m, MonadIO m, MonadCatch m) => m ()
updateAsvoStatus = do
  ServerEnv {..} <- ask

  -- Update the ASVO queue.
  -- If this fails, issue a message and retry.
  asvoQueue      <- try (getAsvoQueue cookieJar) >>= \case
    Right a ->
      -- giant-squid gives the ASVO queue by ASVO Job ID. heracles tracks
      -- obsids; convert here.
      return $ convertAsvoQueue a
    Left (HttpExceptionRequest _ (ConnectionFailure _)) ->
      printLogMessage [chunk "Failed to get ASVO job statuses... Will keep retrying."]
        >> retryUntilAsvoReturns cookieJar
    Left e -> throw e

  (newlyDownloadingDbRows, adjustedDbRows) <- updateDatabaseFromAsvo asvoQueue
                                                                     asvoQueueTVar
                                                                     jobStatusesTVar
                                                                     bufferSize

  -- Submit the download jobs.
  mapM_ (submitAsvoVisibilityJob cookieJar) (V.toList $ _obsid <$> newlyDownloadingDbRows)

  -- Update the SQLite database.
  let allNewDbRows = newlyDownloadingDbRows <> adjustedDbRows
  when (not (V.null allNewDbRows)) $ liftIO $ putMVar databaseMVar $ Existing <$> allNewDbRows

  unless (null newlyDownloadingDbRows) $ printMessage
    [ chunk "Submitted "
    , chunk (tlshow $ V.length newlyDownloadingDbRows) & fore blue
    , chunk " obsids to ASVO for visibility download."
    ]

  let correctedFromAsvoQueue = V.length adjustedDbRows
  unless (correctedFromAsvoQueue == 0) $ printLogMessage
    [ chunk (tlshow correctedFromAsvoQueue) & fore yellow
    , chunk " obsids updated to reflect ASVO status.\n"
    ]

  -- TODO: Print out ASVO errors explicitly.
  when (debug && not (null adjustedDbRows)) $ printLogMessage
    [chunk (tlshow $ (\r -> (_obsid r, _status r)) <$> adjustedDbRows) & fore yellow]


  sleep 60
  updateAsvoStatus
 where
  convertAsvoQueue :: AsvoQueue -> Map Obsid AsvoJob
  convertAsvoQueue jobIdKeyMap =
    let m = M.fromList $ HM.toList jobIdKeyMap in M.mapKeys (\aji -> obsid $ m M.! aji) m
  retryUntilAsvoReturns cookieJar = sleep 60 >> try (getAsvoQueue cookieJar) >>= \case
    Right a ->
      printLogMessage [chunk "Connection to ASVO re-established."] >> return (convertAsvoQueue a)
    Left (HttpExceptionRequest _ (ConnectionFailure _)) -> retryUntilAsvoReturns cookieJar
    Left e -> throw e

{-| Find a ready-to-download obsid and process it. The job must be ready in both
the ASVO queue and our SQLite database.
-}
extractJob :: (MonadReader ServerEnv m, MonadIO m) => m (Maybe Obsid)
extractJob = do
  ServerEnv {..} <- ask
  maybeDbRow     <- liftIO $ atomically $ do
    asvoQueue   <- readTVar asvoQueueTVar
    jobStatuses <- readTVar jobStatusesTVar
    let asvoReady = M.keys $ M.filter (\AsvoJob { jobState } -> jobState == Ready) asvoQueue
        dbRows =
          fmap fromJust
            .   filter (\r -> isJust r && (_status <$> r) == Just Downloading)
            $   (`M.lookup` jobStatuses)
            <$> asvoReady
    case headMay dbRows of
      Nothing    -> return Nothing
      Just dbRow -> do
        let dbRow' = dbRow { _status = DbRow.Processing }
        modifyTVar jobStatusesTVar $ M.insert (_obsid dbRow) dbRow'
        return $ Just dbRow'

  -- Update the database if we found an obsid ready to be processed.
  maybe (return ()) (liftIO . putMVar databaseMVar . V.singleton . Existing) maybeDbRow
  return $ _obsid <$> maybeDbRow

{-| If the client doesn't acknowledge a job that's already been extracted, then we
need to reset the database from "processing" to "downloading". It is
intentional to not check if the obsid is in the Map.
-}
resetJobToDownloading :: (MonadReader ServerEnv m, MonadIO m) => Obsid -> m ()
resetJobToDownloading obsid = do
  ServerEnv {..} <- ask
  dbRow          <- liftIO $ atomically $ do
    jobStatuses <- readTVar jobStatusesTVar
    let dbRow  = jobStatuses M.! obsid
        dbRow' = dbRow { _status = Downloading }
    modifyTVar jobStatusesTVar $ M.insert obsid dbRow'
    return dbRow'
  liftIO $ putMVar databaseMVar $ V.singleton $ Existing dbRow

{-| This function acts as the interface to the SQLite database. `heracles` is
designed such that this is only way to update the database. By using an 'MVar', we
don't have to wait for this function to finish. This function handles the case
of attempting to write to the database when it is locked by retrying after a
small wait.
-}
updateDatabaseThread :: (MonadReader ServerEnv m, MonadIO m) => FilePath -> m ()
updateDatabaseThread dbFile = do
  ServerEnv {..} <- ask
  updatedDbRows  <- liftIO $ takeMVar databaseMVar

  liftIO $ updateRows debug updatedDbRows

  updateDatabaseThread dbFile
 where
  updateRows debug rs =
    liftIO
      $   try
            ( withConnection dbFile
            $ \conn -> withTransaction conn $ mapM_ (insertOrUpdateDBRow conn) rs
            )
      >>= \case
            Right () -> return ()
            Left SQLError { sqlError = e, sqlErrorDetails = d, sqlErrorContext = c } ->
              putStrLn "updateDatabaseThread encountered an SQLError; will try again in 10s"
                >> when
                     debug
                     (  putStrLn ("Error: " ++ show e)
                     >> TIO.putStrLn ("Details: " <> d)
                     >> TIO.putStrLn ("Context: " <> c)
                     )
                >> sleep 10
                >> updateRows debug rs

serverLoop
  :: (MonadReader ServerEnv m, MonadUnliftIO m, MonadThrow m, PrimMonad m) => Socket -> m ()
serverLoop sock = do
  env@ServerEnv {..} <- ask

  clientMessage      <- NS.recv sock 4096

  when debug $ liftIO $ putStrLn $ "Client message: " <> show clientMessage
  case clientMessage of
    Just message -> do
      case message of
        "Give me a job" -> do
          maybeJob <- extractJob
          case maybeJob of
            -- A job is available; send it to the client.
            Just obsid -> do
              when debug $ liftIO $ putStrLn $ concat
                ["Sending job (", show obsid, ") to ", show sock]
              NS.send sock $ bshow obsid
              -- Wait for acknowledgement. If the client does not acknowledge,
              -- then we need to reset this obsid in the database.
              ack <- NS.recv sock 4096
              case ack of
                Just hostName -> printLogMessage
                  [ chunk "Client "
                  , chunk (TL.pack $ B8.unpack hostName)
                  , chunk " is processing "
                  , chunk (tlshow obsid) & fore blue
                  ]
                _ -> do
                  resetJobToDownloading obsid
                  error $ "Unexpected response for acknowledgement (" ++ show ack ++ ") received!"
            Nothing -> do
              when debug $ liftIO $ putStrLn $ concat
                ["Job was requested from ", show sock, ", but there are no jobs ready."]
              NS.send sock "No jobs available"

        "Are you alive?"    -> NS.send sock "Yes!"

        -- This is a security concern, but I think the risk is offset by the
        -- complication of all clients having the exact same key locally
        -- available; it is confusing when things go wrong! I suppose it is also
        -- possible that the environment variable could be edited between the
        -- initial starting of the server, but that's fairly unlikely.
        "MWA ASVO key?"     -> NS.send sock =<< B8.pack <$> liftIO (getEnv "MWA_ASVO_API_KEY")

        "heracles version?" -> NS.send sock $ B8.pack $ showVersion version

        _                   -> if isATenDigitNumberB message
          then do
   -- We've received an obsid. Ask for the job status, and act accordingly.
            when debug $ liftIO $ B8.putStrLn $ "Received obsid: " <> message
            NS.send sock "Job result?"
            result <- NS.recv sock 4096
            case result of
              Nothing -> error $ "Unexpected result (" ++ show result ++ ") received!"
              Just bs -> do
                when debug $ liftIO $ putStrLn "Job result received."
                let dbRow           = either error id (decode bs :: Either String DbRow)
                    obsid           = _obsid dbRow
                    clientDirStatus = _status dbRow

                -- Receive the job files from the client.
                -- Deal with the possibility of the same-named directory already
                -- being present.
                directoryAlreadyPresent <- liftIO $ D.doesDirectoryExist (show obsid)
                if not directoryAlreadyPresent
                  then do
                    when debug
                      $ liftIO
                      $ putStrLn
                          "Client is sending a processing job that we don't have. Asking for processing products."
                    receiveZipFromClient env obsid dbRow
                  else do
                    when debug
                      $ liftIO
                      $ putStrLn
                          "Client is sending a processing job that we already have. Checking our dir's status."
                    existingDirStatus <- jobStatus (show obsid)
                    case existingDirStatus of
                      DbRow.Succeeded -> if clientDirStatus /= DbRow.Succeeded
                        then do
                          when debug $ liftIO $ putStrLn
                            "Rejecting client's dir, because theirs failed, and ours has not."
                          NS.send sock "Zip not wanted"
                        else do
                          when debug $ liftIO $ putStrLn
                            "Client's dir succeeded, but we will reject if ours is newer."
                          NS.send sock "Job date?"
                          ourModTime         <- liftIO $ D.getModificationTime (show obsid)
                          maybeClientModTime <- NS.recv sock 4096
                          case maybeClientModTime of
                            Nothing ->
                              error $ "Unexpected result (" ++ show result ++ ") received!"
                            Just clientModTimeBS ->
                              let
                                clientModTime =
                                  either error id (decode clientModTimeBS :: Either String UTCTime)
                              in
                                if ourModTime > clientModTime
                                  then do
                                    when debug
                                      $ liftIO
                                      $ putStrLn
                                          "Rejecting client's dir, because our successful processing is newer than theirs."
                                    NS.send sock "Zip not wanted"
                                  else do
                                    when debug
                                      $ liftIO
                                      $ putStrLn
                                          "Deleting our older processing dir and asking the client for theirs."
                                    liftIO $ D.removeDirectoryRecursive (show obsid)
                                    receiveZipFromClient env obsid dbRow
                      _ -> do
                        when debug $ liftIO $ putStrLn
                          "Deleting our processing dir, and asking the client for theirs."
                        liftIO $ D.removeDirectoryRecursive (show obsid)
                        receiveZipFromClient env obsid dbRow
                printJobResult False dbRow
          else
            error
            $  "Got a message ("
            ++ B8.unpack message
            ++ "), but not sure what to do with that!"

      serverLoop sock

    Nothing ->
      when debug $ liftIO $ putStrLn $ "Client on socket " ++ show sock ++ " has disconnected."
 where
  updateDatabase ServerEnv {..} obsid dbRow = liftIO $ do
    atomically $ modifyTVar' jobStatusesTVar $ M.insert obsid dbRow
    putMVar databaseMVar $ V.singleton $ Existing dbRow
  receiveZipFromClient env@ServerEnv { debug } obsid dbRow = do
    liftIO $ NS.send sock "Ready to receive zip"
    receiveZip sock
    liftIO $ NS.send sock "Acknowledged"
    when debug $ liftIO $ putStrLn "Products from processing received."
    updateDatabase env obsid dbRow

clientLoop :: (MonadReader ClientEnv m, MonadIO m) => m ()
clientLoop = do
  ClientEnv {..} <- ask

  -- The client will open a new connection to the server every time we loop.
  -- The following code handles connection issues, and will attempt to reconnect
  -- automatically. Elsewhere, we need to manually close the socket when we're done
  -- communicating.
  sock           <- obtainSocketWhenReady serverIP serverPort

  -- Ensure that we're in the data directory.
  liftIO $ D.setCurrentDirectory dataDirectory

  -- Check the data directory for previous processing runs. Iterate over them,
  -- uploading to the server and/or deleting them until they're all gone.
  calDirs <- filter isATenDigitNumber <$> liftIO (D.listDirectory dataDirectory)
  -- Ask the server if they want these directories. Server may reject if the
  -- processing failed, or is older than what the server has.
  forM_ calDirs $ \dir -> do
    status <- jobStatus dir
    when debug
      $  liftIO
      $  putStrLn
      $  "Found previous processing job ("
      <> dir
      <> ") which has status: "
      <> show status
    currentTime <- liftIO getCurrentTime
    let dbRow = blankDbRow (Obsid $ read dir) status (tlshow currentTime)
    uploadJobDir debug cleanupScriptPath sock dbRow dir

  -- Ask for a job number.
  when debug $ liftIO $ putStrLn "Asking for a job from the server"
  NS.send sock "Give me a job"
  response <- NS.recv sock 4096
  when debug $ liftIO $ B8.putStrLn $ "Response: " <> bshow response
  case response of
    Nothing -> printLogMessage [chunk "Server closed the connection."] >> NS.closeSock sock
    Just "No jobs available" -> do
      when verbose $ printLogMessage [chunk "Nothing to do; sleeping...\n"]
      NS.closeSock sock
      sleep 60
    Just maybeObsid -> do
      unless (isATenDigitNumberB maybeObsid)
        $  error
        $  B8.unpack
        $  "Unexpected obsid ( "
        <> maybeObsid
        <> ") from server!"
      hn <- liftIO getHostName
      NS.send sock $ B8.pack hn
      NS.closeSock sock

      let obsid = Obsid $ fst $ fromJust $ B8.readInt maybeObsid
      printLogMessage [chunk "Processing ", chunk (tlshow obsid) & fore blue]
      dbRow <- runProcessJob obsid
      printJobResult True dbRow

      -- Now that the processing job has finished, we need to report it back to the server.
      when debug $ liftIO $ putStrLn "Uploading processing products."
      -- Reconnect to the server to upload our results.
      sock' <- obtainSocketWhenReady serverIP serverPort
      uploadJobDir debug cleanupScriptPath sock' dbRow $ B8.unpack maybeObsid
      NS.closeSock sock'

  weShouldExit <- liftIO $ readTVarIO exitNowTVar
  unless weShouldExit clientLoop
 where
  handleUnexpectedResponse sock expected errorMessage = do
    statusResponse <- NS.recv sock 4096
    unless (statusResponse == expected)
      $  error
      $  "Unexpected server response when "
      ++ errorMessage
      ++ "! Response: "
      ++ show statusResponse
  blankDbRow obsid status currentTime = DbRow { _obsid         = obsid
                                              , _status        = status
                                              , _message       = "old processing dir"
                                              , _lastUpdated   = currentTime
                                              , _timeToProcess = "0"
                                              }
  uploadJobDir _debug cleanupScriptPath sock dbRow dir = do
    when _debug $ liftIO $ putStrLn $ "Sending processing dir " <> dir <> " to the server"
    NS.send sock $ B8.pack dir
    handleUnexpectedResponse sock (Just "Job result?") "requesting job result"
    -- Send it the resulting database row.
    NS.send sock $ encode dbRow
    when _debug $ liftIO $ putStrLn "Status uploaded."
    response <- NS.recv sock 4096
    case response of
      Just "Job date?" -> do
        modTime <- liftIO $ D.getModificationTime dir
        NS.send sock $ encode modTime
        decision <- NS.recv sock 4096
        case decision of
          Just "Zip not wanted"       -> zipNotWantedAction
          Just "Ready to receive zip" -> sendZipAction
          _                           -> unexpectedResponseError decision
      Just "Zip not wanted"       -> zipNotWantedAction
      Just "Ready to receive zip" -> sendZipAction
      _                           -> unexpectedResponseError response
   where
    zipNotWantedAction :: MonadIO m => m ()
    zipNotWantedAction = do
      when _debug
        $  liftIO
        $  putStrLn
        $  "Server does not want our directory for obsid "
        <> dir
        <> "; deleting."
      liftIO $ D.removeDirectoryRecursive dir

    sendZipAction :: MonadIO m => m ()
    sendZipAction = do
      when _debug $ liftIO $ putStrLn "Running cleanup script."
      _ <- liftIO $ P.readProcess cleanupScriptPath [] ""

      when _debug $ liftIO $ putStrLn "Sending directory to server."
      sendDirectory sock dir
      -- Wait for acknowledgement, then delete the job directory.
      handleUnexpectedResponse sock
                               (Just "Acknowledged")
                               "awaiting the server to acknowledge completion"
      liftIO $ D.removeDirectoryRecursive dir
      when _debug $ liftIO $ putStrLn "Deleted the job directory."
    unexpectedResponseError response = error $ "Unexpected response (" ++ show response ++ ")!"

-- | Download, process and delete an observation using the specified script.
downloadProcessDelete :: (MonadReader ClientEnv m, MonadIO m) => Obsid -> m JobStatus
downloadProcessDelete specifiedObsid = do
  ClientEnv {..} <- ask
  asvoQueue      <- liftIO $ getAsvoQueue cookieJar
  let path   = dataDirectory </> show specifiedObsid
      obsid' = show specifiedObsid
      -- Find *any* ASVO job that has "download visibilities", is ready, and the right obsid.
      job =
        case
            HM.elems $ HM.filter
              (\AsvoJob {..} ->
                (obsid == specifiedObsid)
                  && (jobState == Ready)
                  && (jobType == DownloadVisibilities)
              )
              asvoQueue
          of
            []      -> error $ obsid' <> " is not in the asvoQueue! This shouldn't happen."
            (j : _) -> j

  liftIO $ D.createDirectoryIfMissing True path
  liftIO $ D.setCurrentDirectory path

  -- Download from ASVO only when necessary.
  downloadCompleteFile <- liftIO $ D.doesFileExist ".heracles_download_complete"
  noGpuboxFiles        <- null <$> liftIO (G.globDir1 (G.compile "*gpubox*.fits") ".")
  if downloadCompleteFile && not noGpuboxFiles
    then printLogMessage [chunk "Data found; download unnecessary."]
    else
      liftIO (downloadAsvoJob' False cookieJar False False job)
      >> touchHeraclesFile ".heracles_download_complete"
      >> printLogMessage [chunk "Completed visibility download."]
  processingResult <- runProcessWithTimeout (timeLimitMins * 60) "stdout.txt"
    $ unwords [runScriptPath, obsid']

  -- Clean up files.
  _ <- liftIO $ P.readProcess cleanupScriptPath [] ""

  -- Move back into the data directory.
  liftIO $ D.setCurrentDirectory ".."

  case processingResult of
    Nothing              -> return HT.TimedOut
    Just (ExitFailure _) -> return HT.Failed
    Just ExitSuccess     -> do
      -- Say that this job completed successfully.
      touchHeraclesFile $ path </> ".heracles_success"

      return HT.Succeeded

-- | A thin wrapper for `downloadProcessDelete`, that returns more metadata on the job.
runProcessJob :: (MonadReader ClientEnv m, MonadIO m) => Obsid -> m DbRow
runProcessJob obsid = do
  startTime  <- liftIO getCurrentTime
  result     <- downloadProcessDelete obsid
  finishTime <- liftIO getCurrentTime
  let (status, message) = case result of
        HT.Succeeded -> (DbRow.Succeeded, "") :: (DbRowStatus, TL.Text)
        HT.Failed    -> (DbRow.Failed, "script failed")
        HT.TimedOut  -> (DbRow.Failed, "timed out")
        HT.NoData    -> (DbRow.Failed, "no data available")
      dbRow = DbRow { _obsid         = obsid
                    , _status        = status
                    , _message       = message
                    , _timeToProcess = tlshow $ diffUTCTime finishTime startTime
                    , _lastUpdated   = mempty
                    }
  return dbRow

exitOnEnter :: (MonadReader ClientEnv m, MonadIO m) => m ()
exitOnEnter = do
  ClientEnv {..} <- ask

  -- It's possible to send an EOF to getLine (e.g. with Ctrl-D) that causes
  -- getLine to throw an exception. Ignore that exception, while throwing all
  -- others.
  _              <- liftIO (try getLine) >>= \case
    Right _ -> return ()
    Left  e -> if isEOFError e then exitOnEnter else liftIO $ throw e
  weShouldExit <- liftIO (not <$> readTVarIO exitNowTVar)
  liftIO $ atomically $ modifyTVar' exitNowTVar not
  printMessage
    [ chunk "Enter received" & bold & fore yellow
    , chunk ": "
    , chunk (if weShouldExit then "Exiting when ready." else "Will not exit.")
    ]
  exitOnEnter
