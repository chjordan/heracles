-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Heracles.Database
  ( createDatabase
  , getAllRows
  , getRowsWithStatus
  , getProcessing
  , getDownloading
  , getFailed
  , getUnqueued
  , insertObsid
  , insertObsids
  , insertDbRow'
  , updateDBRow
  , updateDBRow'
  , updateDBStatus
  , insertOrUpdateDBRow
  , resetObsid
  , resetObsids
  , databaseToHashMap
  )
where

import           Data.Map.Lazy                            ( Map )
import qualified Data.Map.Lazy                 as M
import           Data.Text.Lazy                           ( Text )
import           Data.Time.Clock                          ( getCurrentTime )
import           Database.SQLite.Simple

import           GiantSquid

import           Heracles.Internal                        ( newDbRow )
import           Heracles.Types.DbRow
import qualified Heracles.Types.DbRow          as DbRow


createDatabase :: Connection -> IO ()
createDatabase conn = execute_
  conn
  "CREATE TABLE IF NOT EXISTS heracles_jobs (\
  \ Obsid INTEGER UNIQUE PRIMARY KEY, \
  \ Status TEXT NOT NULL, \
  \ Message TEXT, \
  \ LastUpdated TEXT NOT NULL, \
  \ TimeToProcess TEXT \
  \ )"

getAllRows :: Connection -> IO [DbRow]
getAllRows conn = query_ conn "SELECT * FROM heracles_jobs"

getRowsWithStatus :: DbRowStatus -> Connection -> IO [DbRow]
getRowsWithStatus status conn =
  queryNamed conn "SELECT * FROM heracles_jobs WHERE Status = :status" [":status" := status]

getProcessing :: Connection -> IO [DbRow]
getProcessing = getRowsWithStatus DbRow.Processing

getDownloading :: Connection -> IO [DbRow]
getDownloading = getRowsWithStatus Downloading

getFailed :: Connection -> IO [DbRow]
getFailed = getRowsWithStatus Failed

getUnqueued :: Connection -> IO [DbRow]
getUnqueued = getRowsWithStatus Unqueued

-- insertObsid doesn't check if the obsid is already in the table!
insertObsid :: Obsid -> Connection -> IO ()
insertObsid obsid conn = do
  currentTime <- getCurrentTime
  let new = newDbRow obsid
  executeNamed
    conn
    "INSERT INTO heracles_jobs \
    \ (Obsid, Status, Message, LastUpdated, TimeToProcess) VALUES \
    \ (:obsid, :status, :message, :lastUpdated, :timeToProcess)"
    [ ":obsid" := obsid
    , ":status" := _status new
    , ":message" := _message new
    , ":lastUpdated" := tshow currentTime
    , ":timeToProcess" := _timeToProcess new
    ]

insertObsids :: [Obsid] -> Connection -> IO ()
insertObsids []     _    = return ()
insertObsids obsids conn = withTransaction conn $ mapM_ (`insertObsid` conn) obsids

insertDbRow' :: DbRow -> Connection -> IO ()
insertDbRow' DbRow {..} conn = do
  currentTime <- getCurrentTime
  executeNamed
    conn
    "INSERT INTO heracles_jobs \
    \ (Obsid, Status, Message, LastUpdated, TimeToProcess) VALUES \
    \ (:obsid, :status, :message, :lastUpdated, :timeToProcess)"
    [ ":status" := _status
    , ":message" := _message
    , ":timeToProcess" := _timeToProcess
    , ":lastUpdated" := tshow currentTime
    , ":obsid" := _obsid
    ]

updateDBStatus :: Text -> Obsid -> Connection -> IO ()
updateDBStatus status obsid conn = do
  currentTime <- getCurrentTime
  executeNamed
    conn
    "UPDATE heracles_jobs \
                      \ SET Status = :status, \
                      \     LastUpdated = :lastUpdated \
                      \ WHERE Obsid = :obsid"
    [":status" := status, ":lastUpdated" := tshow currentTime, ":obsid" := obsid]

updateDBRow :: Text -> Text -> Text -> Int -> Connection -> IO ()
updateDBRow status message timeDeltaText obsid conn = do
  currentTime <- getCurrentTime
  executeNamed
    conn
    "UPDATE heracles_jobs \
    \ SET Status = :status, \
    \     Message = :message, \
    \     TimeToProcess = :timeToProcess, \
    \     LastUpdated = :lastUpdated \
    \ WHERE Obsid = :obsid"
    [ ":status" := status
    , ":message" := message
    , ":timeToProcess" := timeDeltaText
    , ":lastUpdated" := tshow currentTime
    , ":obsid" := obsid
    ]

updateDBRow' :: DbRow -> Connection -> IO ()
updateDBRow' DbRow {..} conn = do
  currentTime <- getCurrentTime
  executeNamed
    conn
    "UPDATE heracles_jobs \
    \ SET Status = :status, \
    \     Message = :message, \
    \     TimeToProcess = :timeToProcess, \
    \     LastUpdated = :lastUpdated \
    \ WHERE Obsid = :obsid"
    [ ":status" := _status
    , ":message" := _message
    , ":timeToProcess" := _timeToProcess
    , ":lastUpdated" := tshow currentTime
    , ":obsid" := _obsid
    ]

insertOrUpdateDBRow :: Connection -> EitherDbRow -> IO ()
insertOrUpdateDBRow conn (New      dbRow) = insertObsid (_obsid dbRow) conn
insertOrUpdateDBRow conn (Existing dbRow) = updateDBRow' dbRow conn

-- | Reset a database row to have the specified status.
resetDatabaseRowWithStatus :: DbRowStatus -> Obsid -> Connection -> IO ()
resetDatabaseRowWithStatus status obsid conn = do
  currentTime <- getCurrentTime
  executeNamed
    conn
    "UPDATE heracles_jobs \
    \ SET Status = :status, \
    \     Message = '', \
    \     TimeToProcess = :timeToProcess, \
    \     LastUpdated = :lastUpdated \
    \ WHERE Obsid = :obsid"
    [ ":status" := status
    , ":lastUpdated" := tshow currentTime
    , ":timeToProcess" := ("0" :: Text)
    , ":obsid" := obsid
    ]

-- | Reset a database row to have the 'Unqueued' status.
resetObsid :: Connection -> Obsid -> IO ()
resetObsid conn obsid = resetDatabaseRowWithStatus Unqueued obsid conn

-- | Reset multiple database rows to have the 'Unqueued' status.
resetObsids :: [Obsid] -> Connection -> IO ()
resetObsids obsids conn = withTransaction conn $ mapM_ (resetObsid conn) obsids

databaseToHashMap :: Connection -> IO (Map Obsid DbRow)
databaseToHashMap conn = do
  allRows <- getAllRows conn
  return $ M.fromList $ (\r -> (_obsid r, r)) <$> allRows
