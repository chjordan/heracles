-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE NumDecimals       #-}
{-# LANGUAGE OverloadedStrings #-}

module Heracles.Internal
  ( getNetworkInterfaces'
  , isATenDigitNumber
  , isATenDigitNumberB
  , jobStatus
  , updateDatabaseAgainstDataDirectory
  , newDbRow
  , sleep
  , tlshow
  , bshow
  , formatTime'
  , getCurrentUtcTime
  , getCurrentUtcTime'
  , getCurrentLocalTime
  , getCurrentLocalTime'
  , touchHeraclesFile
  , printMessageNoEnv
  , runProcessWithTimeout
  , updateDatabaseFromAsvo
  , updateDatabaseFromFileContents
  , sendDirectory
  , receiveZip
  , checkProgramsRun
  , serverRespondsCorrectly
  , obtainSocketWhenReady
  )
where

import           Codec.Archive.Zip.Conduit.UnZip
import           Codec.Archive.Zip.Conduit.Zip
import           Conduit
import           Control.Arrow                            ( (+++) )
import           Control.Concurrent                hiding ( yield )
import qualified Data.ByteString               as B
import qualified Data.ByteString.Char8         as B8
import qualified Data.Conduit.Network          as CN
import           Data.List                                ( isPrefixOf )
import qualified Data.Map.Lazy                 as M
import           Data.Serialize.Text                      ( )
import qualified Data.Set                      as S
import qualified Data.Text                     as T
import qualified Data.Text.Lazy                as TL
import           Data.Time.Clock                          ( UTCTime
                                                          , getCurrentTime
                                                          )
import           Data.Time.Format                         ( FormatTime
                                                          , defaultTimeLocale
                                                          , formatTime
                                                          )
import           Data.Time.LocalTime                      ( LocalTime
                                                          , getCurrentTimeZone
                                                          , utc
                                                          , utcToLocalTime
                                                          )
import qualified Data.Vector                   as V
import           Data.Version                             ( showVersion )
import           Network.Info
import qualified Network.Simple.TCP            as NS
import           Safe                                     ( readMay )
import qualified System.Directory              as D
import           System.Exit
import           System.FilePath.Posix
import           System.IO
import qualified System.Process                as P
import           System.Process.Internals                 ( ProcessHandle(ProcessHandle)
                                                          , ProcessHandle__(OpenHandle)
                                                          )
import           System.Timeout                           ( timeout )

import           GiantSquid

import           Heracles.Imports
import           Heracles.Types.DbRow
import qualified Paths_giant_squid             as GS
import           Paths_heracles                           ( version )


getNetworkInterfaces' :: MonadIO m => m [NetworkInterface]
getNetworkInterfaces' =
  filter (\n -> let iname = name n in not $ "lo" `isPrefixOf` iname || "docker" `isPrefixOf` iname)
    <$> liftIO getNetworkInterfaces

isATenDigitNumber :: String -> Bool
isATenDigitNumber line = length line == 10 && case (readMay line :: Maybe Int) of
  Just _  -> True
  Nothing -> False

isATenDigitNumberB :: ByteString -> Bool
isATenDigitNumberB line = B.length line == 10 && case B8.readInt line of
  Just (_, "") -> True
  _            -> False

-- | If the file ".heracles_success" is in a job directory, then we say it
-- succeeded.
jobStatus :: MonadIO m => FilePath -> m DbRowStatus
jobStatus dir = liftIO (D.doesFileExist $ dir </> ".heracles_success")
  >>= \filePresent -> return $ if filePresent then Succeeded else Failed

-- | Inspect the statuses of jobs in the specified data directory, and return
-- any database rows that should be updated.
updateDatabaseAgainstDataDirectory :: MonadIO m => Map Obsid DbRow -> FilePath -> m [EitherDbRow]
updateDatabaseAgainstDataDirectory databaseContents dataDirectory = do
  obsidDirs <- filter isATenDigitNumber <$> liftIO (D.listDirectory dataDirectory)
  let jobDirs = (\d -> dataDirectory </> d) <$> obsidDirs
  jobDirStatuses <- mapM jobStatus jobDirs
  let
    maybeUpdates =
      (\(o, s) -> case o `M.lookup` databaseContents of
          Nothing ->
            let d = newDbRow o
            in  Just $ New d { _status = s, _message = "updated via data directory" }
          Just dbRow -> if _status dbRow /= s
            then Just $ Existing dbRow { _status = s, _message = "updated via data directory" }
            else Nothing
        )
        <$> zip (read <$> obsidDirs :: [Obsid]) jobDirStatuses
    updates = fromJust <$> filter isJust maybeUpdates
  return updates

newDbRow :: Obsid -> DbRow
newDbRow obsid = DbRow { _obsid         = obsid
                       , _status        = Unqueued
                       , _message       = ""
                       , _timeToProcess = "0"
                       , _lastUpdated   = mempty
                       }

-- | 'threadDelay' in seconds
sleep :: MonadIO m => Int -> m ()
sleep = liftIO . threadDelay . (* 1e6)

-- | Show a value as a lazy 'Text'.
tlshow :: Show a => a -> Text
tlshow = TL.pack . show

-- | Show a value as a 'ByteString'.
bshow :: Show a => a -> ByteString
bshow = B8.pack . show

-- | Custom 'Data.Time.Format.formatTime'.
formatTime' :: FormatTime a => a -> Text
formatTime' = TL.pack . formatTime defaultTimeLocale "%a %b %e %H:%M:%S %Z"

-- | Abstracted 'Data.Time.Clock.getCurrentTime'.
getCurrentUtcTime :: MonadIO m => m UTCTime
getCurrentUtcTime = liftIO getCurrentTime

-- | Custom formatted UTC time.
getCurrentUtcTime' :: MonadIO m => m Text
getCurrentUtcTime' = formatTime' <$> getCurrentUtcTime

-- | An analogue to 'Data.Time.Clock.getCurrentTime', for local time.
getCurrentLocalTime :: MonadIO m => m LocalTime
getCurrentLocalTime = do
  utcT <- getCurrentUtcTime
  tz   <- liftIO getCurrentTimeZone
  return $ utcToLocalTime tz utcT

-- | Custom formatted local time.
getCurrentLocalTime' :: MonadIO m => m Text
getCurrentLocalTime' = do
  lt <- getCurrentLocalTime
  -- When converting UTCTime to LocalTime, the TZ information is lost when using
  -- the %Z formatter. Append it here.
  tz <- liftIO getCurrentTimeZone
  return $ (formatTime' lt) `TL.append` (tlshow tz)

printMessageNoEnv :: MonadIO m => (Chunk a -> [ByteString] -> [ByteString]) -> [Chunk a] -> m ()
printMessageNoEnv terminalCapabilities chunks =
  liftIO . B8.putStrLn . B.concat $ chunksToByteStrings terminalCapabilities chunks

-- | Create a file with the software versions.
touchHeraclesFile :: MonadIO m => FilePath -> m ()
touchHeraclesFile f = do
  liftIO
    $  writeFile f
    $  "heracles version: "
    <> showVersion version
    <> "\ngiant-squid version: "
    <> showVersion GS.version
    <> "\n"

runProcessWithTimeout :: MonadIO m => Int -> FilePath -> String -> m (Maybe ExitCode)
runProcessWithTimeout timeLimitSeconds stdoutFile command =
  liftIO $ withFile stdoutFile WriteMode $ \hOut -> do
    (_, _, _, ph) <- P.createProcess (P.shell command) { P.std_out      = P.UseHandle hOut
                                                       , P.std_err      = P.UseHandle hOut
                                                       , P.create_group = True
                                                       }
    result <- timeout (timeLimitSeconds * 1e6) (P.waitForProcess ph)
    when (isNothing result) $ terminateProcessGroup ph
    _ <- P.waitForProcess ph
    return result
 where
  terminateProcessGroup :: P.ProcessHandle -> IO ()
  terminateProcessGroup (ProcessHandle mh _ _) = readMVar mh >>= \case
    OpenHandle pid -> signalProcessGroup 15 pid
    _              -> return ()

-- Not happy with this function name. It should also update on demand.
updateDatabaseFromAsvo
  :: MonadIO m
  => Map Obsid AsvoJob
  -> TVar (Map Obsid AsvoJob)
  -> TVar (Map Obsid DbRow)
  -> Int
  -> m (Vector DbRow, Vector DbRow)
updateDatabaseFromAsvo asvoQueue asvoTVar databaseTVar bufferSize = liftIO $ atomically $ do
  writeTVar asvoTVar asvoQueue
  jobStatuses <- readTVar databaseTVar
  -- Update the job statuses (which reflects the SQLite database contents).
  -- Database rows might have the status "unqueued", but they're actually on the ASVO queue
  -- with a status queued, downloading, ready or failed. Similarly, database rows might have
  -- the status "downloading", but they're not in the ASVO queue. And etc. for other cases.
  let
  -- Track ASVO failures.
    (asvoFailures, remainder) = M.partition
      (\AsvoJob { jobState } -> case jobState of
        Queued                -> False
        GiantSquid.Processing -> False
        Ready                 -> False
        _                     -> True
      )
      asvoQueue
    failedDownloading =
      M.map (\r -> r { _status = DownloadError })
        $ M.filter (\r -> _status r /= DownloadError)
        $ M.intersection jobStatuses asvoFailures
  -- Update any wrong statuses.
    intersection   = M.intersection jobStatuses remainder
    difference     = M.difference jobStatuses intersection
    areDownloading = M.map (\r -> r { _status = Downloading })
      $ M.filter (\r -> _status r == Unqueued) intersection
    wereDownloading = M.map (\r -> r { _status = Unqueued })
      $ M.filter (\r -> _status r == Downloading) difference
  -- Put all of the adjusted obsids into a new map. This will get to the SQLite
  -- database later. The order specified to M.unions is very important!
  -- `failedDownloading` must come before `areDownloading`, because both
  -- variables are derived using M.intersection, and so potentially have the
  -- same obsids!
    adjustedMap          = M.unions [failedDownloading, wereDownloading, areDownloading]
  -- Use jobStatuses' for the following logic.
    jobStatuses'         = M.union adjustedMap jobStatuses

  -- If there are unqueued observations, *and* there are fewer than `bufferSize` observations
  -- currently downloading, then queue some more.
    unqueued = V.fromList $ M.elems $ M.filter (\r -> _status r == Unqueued) jobStatuses'
    numDownloading       = M.size $ M.filter (\r -> _status r == Downloading) jobStatuses'

    nowDownloadingDbRows = if not (null unqueued) && numDownloading < bufferSize
      then (\r -> r { _status = Downloading }) <$> V.take (bufferSize - numDownloading) unqueued
      else V.empty

  -- Update the in-memory database TVar.
    adjustedDbRows = V.fromList $ M.elems adjustedMap
    newMap =
      M.fromList . V.toList $ (\r -> (_obsid r, r)) <$> (nowDownloadingDbRows <> adjustedDbRows)
    updatedJobStatuses = M.union newMap jobStatuses
  writeTVar databaseTVar updatedJobStatuses

  -- Finally, return the database rows that have been marked for downloading.
  -- They need to be submitted for download in the IO monad.
  -- Also return the database rows that have been adjusted to match their
  -- ASVO status.
  return (nowDownloadingDbRows, adjustedDbRows)

updateDatabaseFromFileContents
  :: MonadIO m
  => TVar (Map Obsid DbRow)
  -> Set Obsid
  -> m (Set Obsid, Set Obsid, Vector EitherDbRow)
updateDatabaseFromFileContents databaseTVar newObsids = liftIO $ atomically $ do
  -- From the obsids tracked by our database (which is reflected by the
  -- databaseTVar), determine which of the newObsids was already in the database
  -- and which are completely new.
  jobStatuses <- readTVar databaseTVar
  let allTrackedObsids = S.fromList $ M.keys jobStatuses
      common           = allTrackedObsids `S.intersection` newObsids
      uncommon         = newObsids S.\\ common
  -- Make new dbRows in the UpdatedDbRow type, to be cleanly passed to the
  -- updateDatabaseThread.
      commonDbRows     = V.map (Existing . newDbRow) $ V.fromList $ S.toList common
      uncommonDbRows   = V.map (New . newDbRow) $ V.fromList $ S.toList uncommon
      toBeSent         = commonDbRows <> uncommonDbRows

  -- Update our in-memory database.
  writeTVar databaseTVar $ M.unions
    [M.fromList $ (\o -> (o, newDbRow o)) <$> S.toList (common <> uncommon), jobStatuses]

  return (common, uncommon, toBeSent)

-- Derived from https://github.com/dylex/zip-stream/blob/master/cmd/zip.hs
generateZip :: (MonadIO m, MonadResource m) => [FilePath] -> ConduitM () (ZipEntry, ZipData m) m ()
generateZip []          = return ()
generateZip (p : paths) = do
  t <- liftIO $ D.getModificationTime p
  let e = ZipEntry { zipEntryName               = Right $ B8.pack $ dropWhile ('/' ==) p
                   , zipEntryTime               = utcToLocalTime utc t -- FIXME: timezone
                   , zipEntrySize               = Nothing
                   , zipEntryExternalAttributes = Nothing
                   }
  isd <- liftIO $ D.doesDirectoryExist p
  if isd
    then do
      dl <- liftIO $ filterM (fmap not . D.pathIsSymbolicLink) . map (p </>) =<< D.listDirectory p
      yield
        ( e { zipEntryName = (`T.snoc` '/') +++ (`B8.snoc` '/') $ zipEntryName e
            , zipEntrySize = Just 0
            }
        , mempty
        )
      generateZip $ dl ++ paths
    else do
      yield (e, zipFileData p)
      generateZip paths

sendDirectory :: MonadIO m => Socket -> FilePath -> m ()
sendDirectory sock dir =
  liftIO
    $  runConduitRes
    $  generateZip [dir]
    .| void (zipStream defaultZipOptions)
    .| CN.sinkSocket sock

receiveZip :: (MonadUnliftIO m, MonadThrow m, PrimMonad m) => Socket -> m ()
receiveZip sock = runConduitRes $ CN.sourceSocket sock .| void unZipStream .| extractZip

{-| Run every input command specified, and return the errors from any
   non-zero-exiting commands.
-}
checkProgramsRun :: MonadIO m => [String] -> m [(String, String)]
checkProgramsRun programs = do
  codes <- forM programs $ \p -> do
    (exitCode, _, pStderr) <- liftIO $ P.readProcessWithExitCode p [] ""
    case exitCode of
      ExitSuccess -> return Nothing
      _           -> return $ Just (p, pStderr)
  return $ fromJust <$> filter isJust codes

serverRespondsCorrectly :: MonadIO m => Socket -> m Bool
serverRespondsCorrectly sock = do
  NS.send sock "Are you alive?"
  response <- NS.recv sock 4096
  return $ response == Just "Yes!"

obtainSocketWhenReady :: MonadIO m => NS.HostName -> NS.ServiceName -> m Socket
obtainSocketWhenReady host port = liftIO $ tryIO (NS.connectSock host port) >>= \case
  Left _ -> do
    putStrLn "Couldn't connect to the server. Will keep retrying!"
    sleep 10
    sock <- waitForReconnect
    putStrLn "Connection re-established!"
    return sock
  Right (sock, _) -> serverRespondsCorrectly sock >>= \c -> if c
    then return sock
    else do
      NS.closeSock sock
      putStrLn "Unexpected response from the server; "
      waitForReconnect
 where
  waitForReconnect :: IO Socket
  waitForReconnect = tryIO (NS.connectSock host port) >>= \case
    Left  _         -> sleep 10 >> waitForReconnect
    Right (sock, _) -> do
      correct <- serverRespondsCorrectly sock
      if correct then return sock else waitForReconnect
