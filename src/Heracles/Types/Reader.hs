-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Heracles.Types.Reader
  (-- | This module defines a class that can be used by multiple various
   -- 'MonadReader' instances.
    HeraclesReader(..)
  )
where

import           Control.Monad.Reader                     ( MonadIO
                                                          , MonadReader
                                                          )
import qualified Data.Text.Lazy                as TL
import           Data.Time.Format                         ( FormatTime )

import           Heracles.Imports


class HeraclesReader a where
  printMessage :: (MonadReader a m, MonadIO m) => [Chunk TL.Text] -> m ()
  printLogMessage :: (MonadReader a m, MonadIO m) => [Chunk TL.Text] -> m ()
  printLogMessage' :: (MonadReader a m, MonadIO m, FormatTime b) => b -> [Chunk TL.Text] -> m ()
