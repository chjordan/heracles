-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}

module Heracles.Types.DbRow
  ( DbRow(..)
  , DbRowStatus(..)
  , EitherDbRow(..)
  )
where

import           Data.Serialize
import           Data.Serialize.Text                      ( )
import           Data.Text                                ( pack )
import qualified Data.Text.Lazy                as TL
import           Database.SQLite.Simple                   ( SQLData(..) )
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.Internal          ( Field(..) )
import           Database.SQLite.Simple.Ok                ( Ok(..) )
import           Database.SQLite.Simple.ToField
import           GHC.Generics

import           GiantSquid


deriving instance Serialize Obsid
deriving instance FromField Obsid
deriving instance ToField Obsid

data DbRow = DbRow
  { _obsid         :: Obsid
  , _status        :: DbRowStatus
  , _message       :: TL.Text
  , _lastUpdated   :: TL.Text
  , _timeToProcess :: TL.Text
  } deriving (Show, Eq, Generic)
instance Serialize DbRow

instance FromRow DbRow where
  fromRow = DbRow <$> field <*> field <*> field <*> field <*> field

data EitherDbRow = New DbRow
                 | Existing DbRow
  deriving (Show)

data DbRowStatus = Unqueued
                 | Downloading
                 | Processing
                 | DownloadError
                 | Succeeded
                 | Failed
  deriving (Eq, Generic)
instance Serialize DbRowStatus

instance Show DbRowStatus where
  show s = case s of
    Unqueued                        -> "unqueued"
    Downloading                     -> "downloading"
    Heracles.Types.DbRow.Processing -> "processing"
    DownloadError                   -> "ASVO downloading error"
    Succeeded                       -> "succeeded"
    Failed                          -> "failed"

instance FromField DbRowStatus where
  fromField (Field (SQLText txt) _) = Ok $ case txt of
    "unqueued"               -> Unqueued
    "downloading"            -> Downloading
    "processing"             -> Heracles.Types.DbRow.Processing
    "ASVO downloading error" -> DownloadError
    "succeeded"              -> Succeeded
    "failed"                 -> Failed
    _                        -> error "Unhandled DbRowStatus"
  fromField _ = error "Couldn't decode DbRowStatus"

instance ToField DbRowStatus where
  toField = SQLText . pack . show
