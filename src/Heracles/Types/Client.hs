-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

module Heracles.Types.Client
  ( ClientEnv(..)
  )
where

import           Control.Concurrent.STM
import           Control.Monad.Reader                     ( ask
                                                          , liftIO
                                                          )
import qualified Data.ByteString.Char8         as B8
import qualified Data.Text.Lazy                as TL

import           Heracles.Imports
import           Heracles.Internal                        ( formatTime'
                                                          , getCurrentLocalTime'
                                                          )
import           Heracles.Types.Reader


data ClientEnv = ClientEnv
  { dataDirectory        :: !FilePath
  , runScriptPath        :: !FilePath
  , cleanupScriptPath    :: !FilePath
  , serverIP             :: !String
  , serverPort           :: !String
  , timeLimitMins        :: {-# UNPACK #-} !Int
  , verbose              :: !Bool
  , debug                :: !Bool
  , cookieJar            :: !CookieJar
  , terminalCapabilities :: Chunk TL.Text -> [ByteString] -> [ByteString]
  , exitNowTVar          :: !(TVar Bool)
  }

instance HeraclesReader ClientEnv where
  printMessage chunks = do
    ClientEnv { terminalCapabilities } <- ask
    liftIO . B8.putStrLn . B8.concat $ chunksToByteStrings terminalCapabilities chunks
  printLogMessage chunks = do
    currentTime <- getCurrentLocalTime'
    printMessage $ [chunk currentTime & bold & fore blue, chunk ": "] <> chunks
  printLogMessage' currentTime chunks =
    printMessage $ [chunk (formatTime' currentTime) & bold & fore blue, chunk ": "] <> chunks
