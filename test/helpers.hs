{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

import           Control.Concurrent.STM
import           Data.Function
import qualified Data.HashMap.Lazy             as HM
import qualified Data.Map.Lazy                 as M
import qualified Data.Set                      as S
import           Test.Hspec

import           GiantSquid
import qualified GiantSquid                    as GS
import           Heracles.Internal
import           Heracles.Types.DbRow                     ( DbRow(..)
                                                          , DbRowStatus(..)
                                                          )


newDbRow' obsid' = let obsid = Obsid obsid' in (obsid, newDbRow obsid)
newDbRowWithStatus status obsid' =
  let obsid = Obsid obsid' in (obsid, (newDbRow obsid) { _status = status })
newDownloadingDbRow = newDbRowWithStatus Downloading
newSucceededDbRow = newDbRowWithStatus Succeeded
newFailedDbRow = newDbRowWithStatus Failed

data Test = Test
  { asvoQueue           :: AsvoQueue
  , databaseContents    :: M.Map Obsid DbRow
  , bufferSize          :: Int
  , numNewlyDownloading :: Int
  , numAdjusted         :: Int
  , beforeAndAfterMatch :: Bool
  }

convertAsvoQueue :: AsvoQueue -> M.Map Obsid AsvoJob
convertAsvoQueue jobIdKeyMap =
  let m = M.fromList $ HM.toList jobIdKeyMap in M.mapKeys (\aji -> obsid $ m M.! aji) m

main :: IO ()
main = hspec $ do
  describe "updateDatabaseFromASVO"
    $ let
        tester Test {..} = do
          let asvoQueue' = convertAsvoQueue asvoQueue
          asvoTVar                                 <- newTVarIO asvoQueue'
          jobStatusesTVar                          <- newTVarIO databaseContents
          -- Run the function we're testing.
          (newlyDownloadingDbRows, adjustedDbRows) <- updateDatabaseFromAsvo asvoQueue'
                                                                             asvoTVar
                                                                             jobStatusesTVar
                                                                             bufferSize
          -- Check the outputs.
          length newlyDownloadingDbRows `shouldBe` numNewlyDownloading
          length adjustedDbRows `shouldBe` numAdjusted
          jobStatuses <- readTVarIO jobStatusesTVar
          if beforeAndAfterMatch
            then jobStatuses `shouldBe` databaseContents
            else jobStatuses `shouldNotBe` databaseContents
        newASVOJob obsid jobId jobType jobState files = AsvoJob { .. }
        -- Pretend that the ASVO job IDs are also the same as the obsids. The
        -- job numbers are not important, but they should be distinct.
        asvoEntry :: Int -> AsvoJobState -> (AsvoJobId, AsvoJob)
        asvoEntry obsid' status =
          let obsid = Obsid obsid'
          in  ( AsvoJobId (unObsid obsid)
              , newASVOJob obsid (AsvoJobId 0) DownloadVisibilities status (Just [])
              )
        readyASVOEntry obsid = asvoEntry obsid Ready
        queuedASVOEntry obsid = asvoEntry obsid Queued
        processingASVOEntry obsid = asvoEntry obsid GS.Processing
        errorASVOEntry obsid = asvoEntry obsid (Error "???")
        expiredASVOEntry obsid = asvoEntry obsid Expired
        cancelledASVOEntry obsid = asvoEntry obsid Cancelled
      in
        do
          it "does nothing when the state in the database matches the ASVO" $ do
            -- The values of the `asvoQueue` keys are not very important here.
            let asvoQueue = HM.fromList [readyASVOEntry 1065880128, readyASVOEntry 1065880248]
                databaseContents =
                  M.fromList
                    [ newDownloadingDbRow 1065880128
                    , newDownloadingDbRow 1065880248
                    , newDbRow' 1065880368
                    ]
            tester $ Test asvoQueue databaseContents 2 0 0 True

          it "detects stale database rows that are no longer downloading on ASVO" $ do
            let asvoQueue        = HM.fromList [readyASVOEntry 1065880128]
                databaseContents = M.fromList
                  [ newDownloadingDbRow 1065880128
                  , newDownloadingDbRow 1065880248
                  , newDownloadingDbRow 1065880368
                  ]
            tester $ Test asvoQueue databaseContents 1 0 2 False

          it "detects unqueued database rows that are downloading on ASVO" $ do
            let
              asvoQueue = HM.fromList
                [readyASVOEntry 1065880128, readyASVOEntry 1065880248, readyASVOEntry 1065880368]
              databaseContents =
                M.fromList
                  [ newDbRow' 1065880128
                  , newDownloadingDbRow 1065880248
                  , newDownloadingDbRow 1065880368
                  ]
            tester $ Test asvoQueue databaseContents 30 0 1 False

          it "submits unqueued database rows" $ do
            let asvoQueue        = HM.fromList [readyASVOEntry 1065880128]
                databaseContents = M.fromList
                  [newDownloadingDbRow 1065880128, newDbRow' 1065880248, newDbRow' 1065880368]
            tester $ Test asvoQueue databaseContents 30 2 0 False

          it "submits unqueued database rows AND corrects an unqueued database row" $ do
            let asvoQueue = HM.fromList [readyASVOEntry 1065880128]
                databaseContents =
                  M.fromList [newDbRow' 1065880128, newDbRow' 1065880248, newDbRow' 1065880368]
            tester $ Test asvoQueue databaseContents 30 2 1 False

          it "handles various ASVO job states" $ do
            -- All ASVO job states specified. All but one database row should be
            -- adjusted; that row should be submitted for download.
            let asvoQueue = HM.fromList
                  [ 1065880128 & queuedASVOEntry
                  , 1065880248 & processingASVOEntry
                  , 1065880368 & readyASVOEntry
                  , 1065880488 & errorASVOEntry
                  , 1065880608 & expiredASVOEntry
                  , 1065880728 & cancelledASVOEntry
                  ]
                databaseContents = M.fromList
                  [ newDbRow' 1065880128
                  , newDbRow' 1065880248
                  , newDbRow' 1065880368
                  , newDbRow' 1065880488
                  , newDbRow' 1065880608
                  , newDbRow' 1065880728
                  , newDbRow' 1065880848
                  ]
            tester $ Test asvoQueue databaseContents 30 1 6 False

          it "does all of the above!" $ do
            -- All ASVO job states specified. All but one database row should be adjusted;
            -- that row should be submitted for download.
            let asvoQueue = HM.fromList
                  [ 1000000001 & queuedASVOEntry
                  , 1000000002 & processingASVOEntry
                  , 1000000003 & readyASVOEntry
                  , 1000000004 & errorASVOEntry
                  , 1000000005 & expiredASVOEntry
                  , 1000000006 & cancelledASVOEntry
                  , 1000000007 & queuedASVOEntry
                  , 1000000008 & processingASVOEntry
                  , 1000000009 & readyASVOEntry
                  , 1000000010 & errorASVOEntry
                  , 1000000011 & expiredASVOEntry
                  , 1000000012 & cancelledASVOEntry
                  ]
                databaseObsids = [1000000001 .. 1000000040] :: [Int]
                databaseContents =
                  M.fromList
                    $   (\o -> if o < 1000000035 then newDbRow' o else newDownloadingDbRow o)
                    <$> databaseObsids
            tester $ Test asvoQueue databaseContents
              -- (12 in ASVO + 6 wrong in database = 18) need to be adjusted.
              -- 6 are already downloading on ASVO, so 14 rows are marked for
              -- downloading (bufferSize = 20).
                                                     20 14 18 False

          it "does all of the above, but with a more complicated database" $ do
            -- All ASVO job states specified. All but one database row should be adjusted;
            -- that row should be submitted for download.
            let asvoQueue = HM.fromList
                  [ 1000000001 & queuedASVOEntry
                  , 1000000002 & processingASVOEntry
                  , 1000000003 & readyASVOEntry
                  , 1000000004 & errorASVOEntry
                  , 1000000005 & expiredASVOEntry
                  , 1000000006 & cancelledASVOEntry
                  , 1000000007 & queuedASVOEntry
                  , 1000000008 & processingASVOEntry
                  , 1000000009 & readyASVOEntry
                  , 1000000010 & errorASVOEntry
                  , 1000000011 & expiredASVOEntry
                  , 1000000012 & cancelledASVOEntry
                  ]
                databaseObsids = [1000000001 .. 1000000050] :: [Int]
                databaseContents =
                  M.fromList
                    $   (\o -> if o < 1000000035
                          then newDbRow' o
                          else if o < 1000000040
                            then newFailedDbRow o
                            else if o < 1000000045 then newSucceededDbRow o else newDownloadingDbRow o
                        )
                    <$> databaseObsids
            tester $ Test asvoQueue databaseContents
              -- (12 in ASVO + 6 wrong in database = 18) need to be adjusted.
              -- 6 are already downloading on ASVO, so 14 rows are marked for
              -- downloading (bufferSize = 20).
                                                     20 14 18 False

          it "does all of the above, but with a more complicated database... again" $ do
            -- All ASVO job states specified. All but one database row should be adjusted;
            -- that row should be submitted for download.
            let asvoQueue = HM.fromList
                  [ 1000000001 & queuedASVOEntry
                  , 1000000002 & processingASVOEntry
                  , 1000000003 & readyASVOEntry
                  , 1000000004 & errorASVOEntry
                  , 1000000005 & expiredASVOEntry
                  , 1000000006 & cancelledASVOEntry
                  , 1000000007 & queuedASVOEntry
                  , 1000000008 & processingASVOEntry
                  , 1000000009 & readyASVOEntry
                  , 1000000010 & errorASVOEntry
                  , 1000000011 & expiredASVOEntry
                  , 1000000012 & cancelledASVOEntry
                  ]
                databaseObsids = [1000000001 .. 1000000020] :: [Int]
                databaseContents =
                  M.fromList
                    $   (\o -> if o < 1000000015
                          then newDbRow' o
                          else if o < 1000000018
                            then newFailedDbRow o
                            else if o < 1000000020 then newSucceededDbRow o else newDownloadingDbRow o
                        )
                    <$> databaseObsids
            tester $ Test asvoQueue databaseContents
              -- (12 in ASVO + 1 wrong in database = 13) need to be adjusted.
              -- bufferSize = 20 -> 6 are already downloading on ASVO, but only 2+1
              -- have a non-final status, so 3 rows are marked for downloading.
                                                     20 3 13 False

  describe "updateDatabaseFromFileContents"
    $ let
        tester databaseContents newObsids' sizeCommon sizeUncommon lengthUpdated numDownloading numDatabaseChanges
          = do
            let newObsids = S.map Obsid newObsids'
            databaseTVar                      <- newTVarIO databaseContents
            -- Run the function we're testing.
            (common, uncommon, updatedDbRows) <- updateDatabaseFromFileContents databaseTVar
                                                                                newObsids
            -- Check the outputs.
            S.size common `shouldBe` sizeCommon
            S.size uncommon `shouldBe` sizeUncommon
            length updatedDbRows `shouldBe` lengthUpdated
            newDatabaseContents <- readTVarIO databaseTVar
            M.size (M.filter (\r -> _status r == Downloading) newDatabaseContents)
              `shouldBe` numDownloading
            M.size
                (M.differenceWith (\a b -> if a == b then Nothing else Just a)
                                  newDatabaseContents
                                  databaseContents
                )
              `shouldBe` numDatabaseChanges
      in
        do
          it "does nothing when there are no new obsids" $ do
            let databaseContents = M.fromList
                  [ newDbRow' 1065880128
                  , newDbRow' 1065880248
                  , newDbRow' 1065880368
                  , newDbRow' 1065880488
                  , newDbRow' 1065880608
                  , newDbRow' 1065880728
                  , newDbRow' 1065880848
                  ]
                newObsids = S.empty
            tester databaseContents newObsids 0 0 0 0 0

          it "detects brand new obsids" $ do
            let databaseContents = M.fromList
                  [ newDbRow' 1065880128
                  , newDbRow' 1065880248
                  , newDbRow' 1065880368
                  , newDbRow' 1065880488
                  , newDbRow' 1065880608
                  , newDbRow' 1065880728
                  , newDbRow' 1065880848
                  ]
                newObsids = S.fromList [1065880968, 1065881088] :: S.Set Int
            tester databaseContents newObsids 0 2 2 0 2

          it "resets obsids already in the database" $ do
            let databaseContents = M.fromList
                  [ newDownloadingDbRow 1065880128
                  , newFailedDbRow 1065880248
                  , newDbRow' 1065880368
                  , newDbRow' 1065880488
                  , newDownloadingDbRow 1065880608
                  , newDbRow' 1065880728
                  , newDbRow' 1065880848
                  ]
                newObsids = S.fromList [1065880128, 1065880248] :: S.Set Int
            tester databaseContents newObsids 2 0 2 1 2

          it "does all of the above!" $ do
            let
              databaseContents = M.fromList
                [ newDownloadingDbRow 1065880128
                , newDbRow' 1065880248
                , newFailedDbRow 1065880368
                , newDownloadingDbRow 1065880488
                , newDbRow' 1065880608
                , newDbRow' 1065880728
                , newDbRow' 1065880848
                ]
              newObsids =
                S.fromList [1065880128, 1065880248, 1065880368, 1065880968, 1065881088] :: S.Set Int
            tester databaseContents newObsids
              -- Three overlapping obsids, two new ones, five new DbRows, but only
              -- four differences between old and new databases (one of the old rows
              -- was already unqueued).
                                              3 2 5 1 4
